# Ideas for how to approach gait generation

Option 1: for every step, search through all graphs.
When match for oldeset value found, find fit.
Repeat for all poitns in graphs, then choose option with best fit.

Option 2: create hash map of "first values".
Each one points to a trie with that value as the root and subsequent values as leaves.
When running, hash oldest value, then go through trie for best fit.
Choose best fit.
Maybe fastest, but worst memory consumption.

Option 3: create set between position and value (x and y), then sort set by y.
When running, find the closest values in log(n) time, then use the index to compare the subsequent values.
